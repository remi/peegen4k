# peeGen4K

also known as "playlist generator"

usage:

`peegen4k.py inputfolder outputfile`

also try:

`peegen4k.py -h` because there's like a few options

generates you a 8-hour playlist of random music files.  
useful for pretending you're a radio station, but you dont have enough storage space to just dump your entire music collection and press shuffle.

requirements:

* python 3.9.12 but newer is Probably Fine
* ffmpeg 5.0.1 but newer is Still Probably Fine
* whatevers in requirements.txt
* some banger tunes