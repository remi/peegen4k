# PROBLEM  
i want tunes

# SOLUTION  
ipod_shuffle.py (now renamed to peegen4k,,)

* inputs is a directory
* program looks thru directory and subfolders for every .mp3 (and flac, and so on) file
* de-duplicates that list (either thru sha-1 or through just going by filename)
    * sha1 route:
        * have array where you store every sha1 of files (loop thru all file)
        * if during your loop, you notice sha1 is already in array, disregard file
    * filename route:
        * have array with all files (maybe arrays in arrays containing folders)
        * loop thru all those, store filename (minus folder name) in a new array
        * if the name is already in array, remove it from the other array
        * OR store them in the new array (with folder somewhere else, somehow (maybe just as an extra array so u can. folder[5] + filename[5] or smth)
* shuffles list
* measures up to ~8 hours of music
* converts into .opus files
    * either as one mega-playlist-file
        * this would work very nicely with pocketchip ipod shuffle app ur gonna make
        * also easy to import to iphone
    * or as a lot of files to just drop on a player
* if mega-playlist-file route, save a txt of when each song plays, so you can refer to that back later
    * in the ipod shuffle app, also have a "note" button that just. saves it. yk
